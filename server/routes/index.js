var express = require('express');
var router = express.Router();
var sanitizer = require('sanitizer');
var db = require("../models/database")();
var jwt = require("jsonwebtoken");

router.post('/signin', function(req, res) {

	// Grab data from http request.Body
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (!req.body.name || !req.body.password || !req.body.email || !emailRegExp.test(req.body.email))
		return res.send();
	else if (req.body.name && req.body.password && req.body.email) {
		var name = req.body.name,
			password = req.body.password,
			email = req.body.email;
		name = sanitizer.escape(name);
		password = sanitizer.sanitize(password);
		email = sanitizer.normalizeRCData(email);

		db.CheckUser(name, email, function(user) {
			// Stream results back
			if (user.email == email) {
				res.json({
					type: false,
					data: "User already exists!"
				});
			} else {
				// sign with default (HMAC SHA256) from JWT
				var token = jwt.sign({
					secret: name
				}, 'shhhhh');
				db.SignUp(name, email, password, token, function(user1) {
					res.json({
						type: true,
						data: user1,
						token: user1.token
					});
				});
			}
		});
	}
});

router.post('/authenticate', function(req, res) {
	// Grab data from http request.Body
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (!req.body.password || !req.body.email || !emailRegExp.test(req.body.email))
		return res.send();
	else if (req.body.password && req.body.email) {
		var password = req.body.password,
			email = req.body.email;
		password = sanitizer.sanitize(password);
		email = sanitizer.normalizeRCData(email);
		db.IsUser(email, password, function(user) {
			if (user.email == email && user.password == password) {
				res.json({
					type: true,
					data: user,
					token: user.token
				});
			} else {
				res.json({
					type: false,
					data: "Incorrect email/password"
				});
			}
		});
	}
});

function ensureAuthorized(req, res, next) {
	var bearerToken;
	var bearerHeader = req.headers["authorization"];
	if (typeof bearerHeader !== 'undefined') {
		var bearer = bearerHeader.split(" ");
		bearerToken = bearer[1];
		req.token = bearerToken;
		next();
	} else {
		res.send(403);
	}
}

/* escape user input  rst so that malicious input, such as the alert() function, cannot be executed.*/

function htmEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

router.get('/me', ensureAuthorized, function(req, res) {
	var token = htmEntities(req.token);
	db.Authenticate(token, function(user) {
		if (user.token != token) {
			res.json({
				type: false,
				data: "Error occured: token auth does no exist"
			});
		} else {
			res.json({
				type: true,
				data: user
			});
		}
	});
});

router.get('/courses', ensureAuthorized, function(req, res) {

	db.Courses(function(user) {
		res.json({
			type: true,
			data: user
		});
	});
});

router.post('/savetimetable', ensureAuthorized, function(req, res) {

	var token = req.token;
	db.Authenticate(token, function(userdetail) {
		// Grab data from http request
		var send_data = JSON.stringify(req.body),
			data = JSON.parse(send_data),
			course_id = 0,
			totalCredit = 0;
		for (var i = 0; i < data.length; i++) {
			totalCredit += data[i].credits;
		}
		if (totalCredit != 15) {
			return res.send();
		} else {
			db.applications(userdetail.id, function(appid) {
				var apid = appid.id;
				console.log(apid);
				for (var i = 0; i < data.length; i++) {
					course_id = data[i].id;
					db.CompletedTimeTale(course_id, apid, function(data) {});
				}
			});
			res.json({
				type: true,
				data: "Successfuly added"
			});
		}
	});
});

router.get('/getTimetable/:tagId', ensureAuthorized, function(req, res) {

	var token = req.token;
	db.Authenticate(token, function(userdetail) {
		if (!userdetail.id) {
			return res.send();
		} else {
			var send_data = req.params.tagId;
			send_data = htmEntities(send_data);
			db.getTimetable(send_data, function(data) {
				res.json({
					type: true,
					data: data
				});
			});
		}
	});
});


router.get('/application', ensureAuthorized, function(req, res) {

	var token = req.token;
	db.Authenticate(token, function(userdetail) {
		if (!userdetail.id) {
			return res.send();
		} else {
			db.GetCompletedTimeTale(userdetail.id, function(data) {
				res.json({
					type: true,
					data: data
				});
			});
		}
	});
});

module.exports = router;
