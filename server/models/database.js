/* database set up */
var pg = require('pg');
var path = require('path');
var connectionString = require(path.join(__dirname, '../', 'config'));

var client = new pg.Client(connectionString);
client.connect();

module.exports = function() {
	return {
		SignUp: function(name, email, password, token, callback) {
			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		CheckUser: function(name, email, callback) {

			var results = {};
			// Get a Postgres client from the connection pool
			pg.connect(connectionString, function(err, client, done) {
				// Handle connection errors
				if (err) {
					done();
					return callback(json({
						success: false,
						data: err
					}));
				}

				// SQL Query > Select Data
				var query = client.query("SELECT * FROM users WHERE name=($1) OR email=($2) ", [name, email]);

				// Stream results back one row at a time
				query.on('row', function(row) {
					results = row;
				});

				// After all data is returned, close connection and return results
				query.on('end', function() {
					done();
					return callback(results);
				});
			});
		},
		Authenticate: function(token, callback) {

			var results = {};
			// Get a Postgres client from the connection pool
			pg.connect(connectionString, function(err, client, done) {
				// Handle connection errors
				if (err) {
					done();
					return callback(json({
						success: false,
						data: err
					}));
				}

				// SQL Query > Select Data
				var query = client.query("SELECT * FROM users WHERE token=($1) ", [token]);

				// Stream results back one row 
				query.on('row', function(row) {
					results = row;
				});

				// After all data is returned, close connection and return results
				query.on('end', function() {
					done();
					return callback(results);
				});
			});
		},
		IsUser: function(email, password, callback) {

			var results = {};
			// Get a Postgres client from the connection pool
			pg.connect(connectionString, function(err, client, done) {
				// Handle connection errors
				if (err) {
					done();
					return callback(json({
						success: false,
						data: err
					}));
				}

				// SQL Query > Select Data
				var query = client.query("SELECT * FROM users WHERE email=($1) AND password=($2) ", [email, password]);

				// Stream results back one row at a time
				query.on('row', function(row) {
					results = row;
				});

				// After all data is returned, close connection and return results
				query.on('end', function() {
					done();
					return callback(results);
				});
			});
		},
		Courses: function(callback) {

			var results = {};
			// SQL Query > Select Data
			var sql = ' SELECT json_agg(t) FROM ( SELECT id, name, credits, (SELECT COUNT(*) FROM classes WHERE course_id= courses.id) as count, ';
			sql += ' (SELECT end_at - start_at as c FROM classes WHERE course_id= courses.id LIMIT 1) as Hours, ';
			sql += ' (SELECT start_at FROM classes WHERE course_id= courses.id LIMIT 1 ) as StartTime, ';
			sql += ' (SELECT end_at FROM classes WHERE course_id= courses.id  LIMIT 1 ) as EndTime, ';
			sql += ' (SELECT array_to_json(array_agg(row_to_json(c))) FROM (SELECT day FROM classes WHERE course_id= courses.id ) c) as Day FROM courses ) t; ';
			var query = client.query(sql);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row.json_agg;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		Classes: function(callback) {

			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		GetClasses: function(callback) {

			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		GetCourses: function(callback) {

			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		Credits: function(callback) {

			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		GetCredits: function(callback) {

			var results = {};
			// SQL Query > Select Data
			client.query("INSERT INTO users(name, email, password, token) values($1, $2, $3, $4) LIMIT 1", [name, email, password, token]);

			// Stream results back one row at a time
			// SQL Query > Select Data
			var query = client.query("SELECT * FROM users WHERE email =($1) ", [email]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		GetCompletedTimeTale: function(uid, callback) {
			var results = {};
			// SQL Query > Select Data
			var sql = ' SELECT json_agg(t) FROM ( SELECT id, (SELECT array_to_json(array_agg(row_to_json(c))) FROM (SELECT name FROM courses INNER JOIN listings ';
			sql += ' ON (courses.id = listings.course_id) WHERE listings.application_id = applications.id ) c) as courses ';
			sql += '	 FROM  applications WHERE created_by = $1 ) t ';
			//sql += '  FROM  applications WHERE created_by = "'+uid+'" ) t; ';
			var query = client.query(sql, [uid]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row.json_agg;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		},
		applications: function(uid, callback) {

			var result = 0;
			// SQL Query > INsert Data
			query = client.query("INSERT INTO applications(created_by) values($1) RETURNING id ", [uid]);
			// Stream results back one row (id)
			query.on('row', function(row) {
				result = row;
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(result);
			});

		},
		CompletedTimeTale: function(course_id, appid, callback) {

			var results = {};
			// SQL Query > INsert Data
			client.query("INSERT INTO listings( course_id , application_id) values( " + course_id + ", " + appid + ") LIMIT 1");
		},
		getTimetable: function(id, callback){
			var results = {};
			// SQL Query > Select Data
			/*var sql = ' SELECT json_agg(t) FROM ( SELECT listings.id,  ';
			sql += ' (SELECT name FROM courses INNER JOIN classes ON (courses.id = classes.course_id) WHERE courses.id = listings.course_id LIMIT 1 ) as text,';
			sql += ' (SELECT start_at FROM classes INNER JOIN courses ON (courses.id = classes.course_id) WHERE courses.id = listings.course_id LIMIT 1 ) as star,';
			sql += ' (SELECT end_at FROM classes INNER JOIN courses ON (courses.id = classes.course_id) WHERE courses.id = listings.course_id LIMIT 1 ) as en';
			sql += ' FROM listings  INNER JOIN courses ON (courses.id = listings.course_id) WHERE listings.application_id =$1 LIMIT 22) t; ';*/
			
		     var sql = 'SELECT json_agg(t) FROM (SELECT classes.id, classes.start_at as star, classes.end_at as en , courses.name as text ';
			sql +='FROM classes  INNER  JOIN ';
			sql +='courses ON (classes.course_id = courses.id)  INNER JOIN  ';
			sql +='listings ON (courses.id = listings.course_id)  WHERE listings.application_id =$1 UNION  ';
			sql +='SELECT classes.id, classes.start_at as star, classes.end_at as en , courses.name as text FROM classes  INNER  JOIN  ';
			sql +='courses ON (classes.course_id = courses.id) INNER JOIN ';
			sql +='listings ON (courses.id = listings.course_id)  WHERE listings.application_id =$1) t ';
		    
		    
			var query = client.query(sql, [id]);

			// Stream results back one row at a time
			query.on('row', function(row) {
				results = row.json_agg;
				console.log(row);
			});

			// After all data is returned, close connection and return results
			query.on('end', function() {
				return callback(results);
			});
		}
	}

}
