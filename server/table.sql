create table users (
  id serial primary key,
  created_at timestamp default current_timestamp,
  name character varying(64),
  email character varying(84),
  password character varying(84),
  token character varying(255)
  
);

create table courses (
  id serial primary key,
  created_at timestamp default current_timestamp,
  name character varying(84),
  credits integer
);

create table classes (
  id serial primary key,
  day character varying(10),
  start_at timestamp,
  end_at timestamp,
  course_id integer references courses (id)
);

create table applications (
  id serial primary key,
  created_at timestamp default current_timestamp,
  created_by integer references users (id)
);

create table listings (
  id serial primary key,
  created_at timestamp default current_timestamp,
  course_id integer references courses (id),
  application_id integer references applications (id)
);



