
 
 #  simple class timetable "manager" for Computer Science students


 This is a basic simple timetable "manager" application  built with Node, Express ,AngularJS and PostgreSQL.


 ## Quick Start

 2. Install dependencies: `npm install`
 3. Start your Postgres server and create a database called "timetable"
 4. Create the database tables using table.sql and dum some data on using data.sql
 6. Go to folder " config" change 
 7. var conString = "postgres://username:password@localhost/database"; with your credentials
 5. Start the server go to timer/server in the command line then hit: `$ node server`
 6. Start the client go to client/ngClient in the command line then hit: `$ node client`
 7. Go to -> http://localhost:3001/ to found the first solution .

 ## Missing Tests
