'use strict';

/* Controllers */

angular.module('timet')
	.directive('courses', function() {
		return {
			restrict: 'E',
			replace: true,
			transclude: true,
			scope: false,
			templateUrl: 'partials/courses.html'
		}
	})
	.directive('progressBar', [
		function() {

			return {
				restrict: 'E',
				scope: {
					curVal: '@',
					maxVal: '@'
				},
				template: "<div class='progress-bar'>" + "<div class='progress-bar-bar'>{{curVal}}/{{maxVal}}</div>" + "</div>",

				link: function($scope, element, attrs) {

					function updateProgress() {
						var progress = 0;

						if ($scope.maxVal) {
							progress = Math.min($scope.curVal, $scope.maxVal) / $scope.maxVal * element.find('.progress-bar').width();
						}

						element.find('.progress-bar-bar').css('width', progress);
					}

					$scope.$watch('curVal', updateProgress);
					$scope.$watch('maxVal', updateProgress);
				}
			};
		}
	])
	.controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main',
		function($rootScope, $scope, $location, $localStorage, Main) {
			$scope.signin = function() {
				var formData = {
					email: $scope.email,
					password: $scope.password
				}

				Main.signin(formData, function(res) {
					if (res.data.token) {
						$localStorage.token = res.data.token;
						$location.path('/me');

					} else {
						$scope.error = res.data;
					}
				}, function() {
					$rootScope.error = 'Failed to signin';
				})
			};

			$scope.signup = function() {
				// check to make sure the form is completely valid
				if ($scope.userForm.$valid) {
					var formData = {
						name: $scope.user.name,
						email: $scope.user.email,
						password: $scope.user.password
					}
					Main.save(formData, function(res) {
						if (res.data.token) {
							$localStorage.token = res.data.token;
							$location.path('/me');

						} else {
							$scope.error = res.data;
						}

					}, function() {
						$scope.error = 'Failed to signup';
					})
				}
			};

			$scope.me = function() {
				Main.me(function(res) {
					$scope.myDetails = res;
				}, function() {
					$rootScope.error = 'Failed to fetch details';
				})
			};

			$scope.logout = function() {
				Main.logout(function() {
					$location.path('/');
				}, function() {
					$rootScope.error = 'Failed to logout';
				});
			};
		}
	])

.controller('MeCtrl', ['$rootScope', '$scope', '$location', 'Main', '$interval',
	function($rootScope, $scope, $location, Main, $interval) {
		/*** tap  **/
		this.tab = 1;
		this.selectTab = function(setTab) {
			this.tab = setTab;
		};
		this.isSelected = function(checkTab) {

			return this.tab === checkTab;
		};

		$scope.myDetails = [];
		Main.me(function(res) {
			$scope.myDetails = res.data;
		}, function() {
			$rootScope.error = 'Failed to fetch User details';
		});

		$scope.courses = [];
		Main.courses(function(res) {
			$scope.courses = res.data;
		}, function() {
			$rootScope.error = 'Failed to fetch course details';
		});

		$scope.successMessage = "false";
		$scope.curVal = 0;
		$scope.maxVal = 15;
		$scope.pickedcourses = [];
		$scope.modalShown = false;
		$scope.setMaster = function($index) {
			//$scope.selectedd = section;

			var a = $scope.courses[$index];
			var b = $scope.curVal + $scope.courses[$index].credits;
			var timeok = true;

			for (var i = 0; i < $scope.pickedcourses.length; i++) {
				if (($scope.courses[$index].starttime == $scope.pickedcourses[i].starttime) && ($scope.courses[$index].endtime = $scope.pickedcourses[i].endtime)) {
					timeok = false;
					break;
				}
			}
			if (timeok == false) {
				$scope.modalShown = !$scope.modalShown;
				$scope.errorMessage = "Time already field at this period.";
			}
			if ($scope.maxVal < b) {
				$scope.modalShown = !$scope.modalShown;
				$scope.errorMessage = " You're only allow  15 credits.";
			} else if ($scope.maxVal >= b && timeok == true) {
				$('.white-panel a[data-id="' + $index + '"]').addClass('disabled');
				$('.white-panel button[data-id="' + $index + '"]').show('slow');

				$scope.pickedcourses.push(a);
				$scope.curVal += $scope.courses[$index].credits;
			}

		};

		$scope.resetMaster = function($index) {
			//$scope.selectedd = section;
			$('.white-panel a[data-id="' + $index + '"]').removeClass('disabled');
			$('.white-panel button[data-id="' + $index + '"]').css('display', 'none');
			var a = $scope.courses[$index];
			$scope.pickedcourses.pop(a);
			$scope.curVal -= $scope.courses[$index].credits;

		};

		$scope.sendTimeTable = function() {

			Main.saveTimeTable($scope.pickedcourses, function(res) {
				if (res.type == true) {

					$scope.curVal = 0;
					$scope.maxVal = 15;
					$scope.pickedcourses = [];
					$('.white-panel a[data-id]').removeClass('disabled');
					$('.white-panel button[data-id]').css('display', 'none');
					$scope.successMessage = "Saving successfull";
					$scope.modalShown = !$scope.modalShown;
				} else {
					$scope.modalShown = !$scope.modalShown;
					$scope.errorMessage = "An error as occur on saveTimeTable.";
				}

			}, function() {
				$scope.modalShown = !$scope.modalShown;
				$scope.errorMessage = "An error as occur on server while saving saveTimeTable.";
			});



		};

		$scope.application = {};
		$scope.getApplication = function() {
			Main.getApplication(function(res) {
				if (res.type == true) {
					$scope.application = res.data;
					console.log($scope.application);
				} else {
					$scope.modalShown = !$scope.modalShown;
					$scope.errorMessage = "An error as occur: getApplication.";
				}

			}, function() {
				$scope.modalShown = !$scope.modalShown;
				$scope.errorMessage = "An error : getApplication.";
			});
		};

		$scope.logout = function() {
			Main.logout(function() {
				$location.path('/');
			}, function() {
				$rootScope.error = 'Failed to logout';
			});
		};

	}
])
	.controller('CalCtrl', ['$rootScope', '$scope', '$location', 'Main', '$interval', '$routeParams', '$timeout', '$http',
		function($rootScope, $scope, $location, Main, $interval, $routeParams, $timeout, $http) {
			$scope.id = $routeParams.id;
			//console.log($scope.id);
			$scope.myDetails = [];
			Main.me(function(res) {
				$scope.myDetails = res.data;
			}, function() {
				$rootScope.error = 'Failed to fetch User details';
			});

			$scope.logout = function() {
				Main.logout(function() {
					$location.path('/');
				}, function() {
					$rootScope.error = 'Failed to logout';
				});
			};


			$scope.events = [];

			$scope.navigatorConfig = {
				selectMode: "day",
				showMonths: 3,
				skipMonths: 3,
				onTimeRangeSelected: function(args) {
					$scope.weekConfig.startDate = args.day;
					$scope.dayConfig.startDate = args.day;
					loadEvents();
				}
			};

			$scope.dayConfig = {
				visible: false,
				viewType: "Day"
			};

			$scope.weekConfig = {
				visible: true,
				viewType: "Week"
			};

			$scope.showDay = function() {
				$scope.dayConfig.visible = false;
				$scope.weekConfig.visible = true;
				$scope.navigatorConfig.selectMode = "day";
			};

			$scope.showWeek = function() {
				$scope.dayConfig.visible = false;
				$scope.weekConfig.visible = true;
				$scope.navigatorConfig.selectMode = "week";
			};

			loadEvents();


			function getMonday(d) {
				d = new Date(d);
				var day = d.getDay(),
					diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
				var n = new Date(d.setDate(diff));
				return n.getDate();
			}

			function getT(d) {
				d = new Date(d);
				var day = d.getDay(),
					diff = d.getDate() - day + (day == 0 ? -6 : 2); // adjust when day is sunday
				var n = new Date(d.setDate(diff));
				return n.getDate();
			}


			function getW(d) {
				d = new Date(d);
				var day = d.getDay(),
					diff = d.getDate() - day + (day == 0 ? -6 : 3); // adjust when day is sunday
				var n = new Date(d.setDate(diff));
				return n.getDate();
			}


			function getTH(d) {
				d = new Date(d);
				var day = d.getDay(),
					diff = d.getDate() - day + (day == 0 ? -6 : 4); // adjust when day is sunday
				var n = new Date(d.setDate(diff));
				return n.getDate();
			}


			function getF(d) {
				d = new Date(d);
				var day = d.getDay(),
					diff = d.getDate() - day + (day == 0 ? -6 : 5); // adjust when day is sunday
				var n = new Date(d.setDate(diff));
				return n.getDate();
			}


			function loadEvents() {
				// using $timeout to make sure all changes are applied before reading visibleStart() and visibleEnd()
				$timeout(function() {
					var params = {
						start: $scope.week.visibleStart().toString(),
						end: $scope.week.visibleEnd().toString()
					}

					Main.getTimetable($scope.id, function(res) {
						if (res.type == true) {
							var x = res.data;
							var a = [];
							var b = '',
								c = '',
								d = '',
								c;
							console.log(x);

							for (var i = 0; i < x.length; i++) {
								var star = x[i].star;
								var en = x[i].en;
								var date = new Date();
								var year = date.getFullYear()
								var day = date.getDay();
								var month = date.getMonth() + 1;
								var dayOfMonth = date.getDate();

								b = star.replace('2016', year);
								c = b.replace('-01-', '-0' + month + '-');
								var n = c.search("01T");
								var n1 = c.search("02T");
								var n2 = c.search("03T");
								var n3 = c.search("04T");
								var n4 = c.search("05T");

								if (n != -1) {
									var mo = getMonday(new Date());
									d = c.replace("01T", mo + 'T');
									x[i].star = d;
								}
								if (n1 != -1) {
									var t = getT(new Date());
									d = c.replace("02T", t + 'T');
									x[i].star = d;
								}
								if (n2 != -1) {
									var we = getW(new Date());
									d = c.replace("03T", we + 'T');
									x[i].star = d;
								}
								if (n3 != -1) {
									var th = getTH(new Date());
									d = c.replace("04T", th + 'T');
									x[i].star = d;
								}
								if (n4 != -1) {
									var fr = getF(new Date());
									d = c.replace("05T", fr + 'T');
									x[i].star = d;

								}

								x[i].start = x[i]['star'];
								delete x[i].star;



								var b1 = en.replace('2016', year),
									c1 = b1.replace('-01-', '-0' + month + '-'),
									d1 = '';
								var n1 = c1.search("01T");
								var n11 = c1.search("02T");
								var n21 = c1.search("03T");
								var n31 = c1.search("04T");
								var n41 = c1.search("05T");

								if (n1 != -1) {
									var mo1 = getMonday(new Date());
									d1 = c1.replace("01T", mo1 + 'T');
									x[i].en = d1;
								}
								if (n11 != -1) {
									var t1 = getT(new Date());
									d1 = c1.replace("02T", t1 + 'T');
									x[i].en = d1;
								}
								if (n21 != -1) {
									var we1 = getW(new Date());
									d1 = c.replace("03T", we1 + 'T');
									x[i].en = d1;
								}
								if (n31 != -1) {
									var th1 = getTH(new Date());
									d1 = c1.replace("04T", th + 'T');
									x[i].en = d1;
								}
								if (n41 != -1) {
									var fr1 = getF(new Date());
									d1 = c1.replace("05T", fr1 + 'T');
									x[i].en = d1;
								}
								x[i].end = x[i]['en'];
								delete x[i].en;


							}
							$scope.events = x;
						} else {
							$scope.modalShown = !$scope.modalShown;
							$scope.errorMessage = "An error as occur: getApplication.";
						}

					}, function() {
						$scope.modalShown = !$scope.modalShown;
						$scope.errorMessage = "An error : getApplication.";
					});





				});
			}


		}




	])
	.directive('modalDialog', function() {
		return {
			restrict: 'E',
			scope: {
				show: '='
			},
			replace: false, // Replace with the template below
			transclude: true, // we want to insert custom content inside the directive
			link: function(scope, element, attrs) {
				scope.dialogStyle = {};
				if (attrs.width)
					scope.dialogStyle.width = attrs.width;
				if (attrs.height)
					scope.dialogStyle.height = attrs.height;
				scope.hideModal = function() {
					scope.show = false;
				};
			},
			template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
		};
	});
